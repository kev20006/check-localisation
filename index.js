const fs = require('fs');
const axios = require('axios').default;


// input json path
const clientData = JSON.parse(fs.readFileSync(__dirname + '/src/inputStrings.json'));

// existing localisation file
// const existingL10n = JSON.parse(fs.readFileSync(__dirname + '/src/fromAPI.json'));

fs.writeFileSync(__dirname + '/output/errors.html', '');

/*
let missingStrings = {}
let duplicateKeys = {}
*/

async function getLocalisation(key, languageCode, string) {

    return axios(`https://api-free.deepl.com/v2/translate?auth_key=eb0fada0-9c0e-0cde-90a8-67b194ec3248:fx&text=${replaceMarkers(string)}&target_lang=${languageCode}&tag_handling=xml`)
    .then(({data}) => {
        console.log(`${key}: translated ✅`)
        return {[key]: restoreMarkers(data.translations[0].text)}
    })
    .catch(err => {})
   
}    

function writeToFile(filename, strings) {
    fs.writeFileSync(__dirname + `/output/nativeStrings/${filename}.json`, JSON.stringify(strings))
}

/*
function getMissingValues() {
    Object.keys(clientData).forEach((key) => {
        if(!existingL10n[key])
            missingStrings = {...missingStrings, [key]: clientData[key]}
    })
    console.log(missingStrings)
}

function getDuplicateValues() {
    Object.keys(clientData).forEach((key) => {
        if(existingL10n[key]) {
            if(existingL10n[key] !== clientData[key]) {
                duplicateKeys = {...duplicateKeys, [key]: clientData[key]}
            }
        }
            
    })
    console.log(duplicateKeys)
}
*/

async function getLocalistationDictionary(languageCode) {
    let newLanguage = {};
    const localisedValues = await Promise.all(Object.keys(clientData).map((key) => getLocalisation(key, languageCode, clientData[key])))
    .then((values) => values);

    
    localisedValues.forEach(value => {
        newLanguage = {...newLanguage, ...value}
    })

    return newLanguage
}
//  lt, ro, 
async function main() {
    const language = ['pt', 'da', 'fr', 'nl', 'lt', 'ro', 'pl', 'bg', 'sk', 'lv', 'hu', 'zh', 'de', 'fi']
    for(l of language) {
        const val = await getLocalistationDictionary(l);
        writeToFile(`${l}-localisation`, val.toJSON ? val.toJSON() : val)
    }
}   

function replaceMarkers(string) {
    return string.split('[').join('<').split(']').join('>');
}

function restoreMarkers(string) {
    return string.split('<').join('[').split('>').join(']');
}

main('IT');


