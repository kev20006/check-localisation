const fs = require('fs');

const BG = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/BG-localisation.json'));
const DA = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/DA-localisation.json'));
const DE = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/DE-localisation.json'));
const HU = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/HU-localisation.json'));
const LT = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/LT-localisation.json'));
const LV = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/LV-localisation.json'));
const NL = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/NL-localisation.json'));
const PL = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/PL-localisation.json'));
const RO = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/RO-localisation.json'));
const SK = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/SK-localisation.json'));
const ZH = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/ZH-localisation.json'));
const FI = JSON.parse(fs.readFileSync(__dirname + '/output/nativeStrings/FI-localisation.json'));
const EN = JSON.parse(fs.readFileSync(__dirname + '/src/inputStrings.json'));

var logger = fs.createWriteStream(__dirname +'/csv/newStrings.csv')

function getValue(key, language) {
    return language[key] ? language[key] : ''
}
console.log('generating csv file 👷‍♂️👷‍♀️')

logger.write(`key \t en \t da \t nl \t de \t fi \t lt \t ro \t pl \t bg \t sk \t lv \t hu \t zh\n`);
Object.keys(EN).forEach((key) => {
    logger.write(`${key} \t ${getValue(key, EN)} \t ${getValue(key, DA)} \t ${getValue(key, NL)} \t ${getValue(key, DE)} \t ${getValue(key, FI)} \t ${getValue(key, LT)} \t ${getValue(key, RO)} \t ${getValue(key, PL)} \t ${getValue(key, BG)} \t ${getValue(key, SK)} \t ${getValue(key, LV)} \t ${getValue(key, HU)} \t ${getValue(key, ZH)}\n`)
})
console.log('finished 🎉')